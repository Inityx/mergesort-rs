#![feature(fn_traits, test, maybe_uninit)]

use std::{
    cmp::Ordering,
    mem::{swap, MaybeUninit},
    iter::repeat_with,
};

use unchecked_unwrap::UncheckedUnwrap;

pub trait MergeSortExt<T>
where
    Self: AsMut<[T]>,
    T: Send,
{
    fn mergesort_by<C>(&mut self, compare: C)
    where C: Sync + Fn(&T, &T) -> Ordering
    {
        let source = self.as_mut();
        let mut back_buffer = repeat_with(MaybeUninit::uninitialized)
            .take(source.len())
            .collect::<Vec<_>>()
            .into_boxed_slice();

        mergesort_with(back_buffer.as_mut(), source, &compare);
    }

    fn mergesorted_by<C>(mut self, compare: C) -> Self
    where Self: Sized, C: Sync + Fn(&T, &T) -> Ordering
    {
        self.mergesort_by(compare);
        self
    }

    fn mergesort(&mut self) where T: Ord {
        self.mergesort_by(Ord::cmp);
    }

    fn mergesorted(self) -> Self
    where Self: Sized, T: Ord
    {
        self.mergesorted_by(Ord::cmp)
    }
}

impl<T, U> MergeSortExt<T> for U where U: AsMut<[T]>, T: Send {}

fn mergesort_with<T: Send>(
    back_buffer: &mut [MaybeUninit<T>],
    slice: &mut [T],
    compare: &(impl Sync + Fn(&T, &T) -> Ordering),
) {
    // 1-item and empty lists are already sorted
    if slice.len() < 2 { return; }

    debug_assert_eq!(slice.len(), back_buffer.len());

    let split = slice.len() / 2;
    let (src_left, src_right) = slice      .split_at_mut(split);
    let (buf_left, buf_right) = back_buffer.split_at_mut(split);

    rayon::join(
        || mergesort_with(buf_left,  src_left,  compare),
        || mergesort_with(buf_right, src_right, compare),
    );

    merge_with(back_buffer, slice, split, compare);
}

fn merge_with<T: Send>(
    back_buffer: &mut [MaybeUninit<T>],
    slice: &mut [T],
    split: usize,
    compare: impl Fn(&T, &T) -> Ordering,
) {
    debug_assert_eq!(slice.len(), back_buffer.len());
    let (left, right) = slice.split_at_mut(split);

    let mut left  = left .iter_mut().peekable();
    let mut right = right.iter_mut().peekable();

    // merge into back buffer
    for slot in &mut *back_buffer {
        use self::Ordering::*;
        let item = match (left.peek(), right.peek()) {
            (Some(l), Some(r)) =>
                match compare(l, r) {
                    Less | Equal => left .next(),
                    Greater      => right.next(),
                }
            (Some(_), None   ) => left .next(),
            (None,    Some(_)) => right.next(),
            (None,    None   ) => unreachable!(),
        };

        unsafe { swap(
            slot.get_mut(), // Safe because we are not interacting with the uninitialized value before it is moved back
            item.unchecked_unwrap(), // Safe because all reachable match arms return `Some`
        ) };
    }

    use rayon::prelude::*;
    // Move to source buffer
    back_buffer
        .par_iter_mut()
        .map(|item| unsafe { item.get_mut() }) // Safe because all values are initialized after merging
        .zip(slice)
        .for_each(|(buf_item, slice_item)| swap(buf_item, slice_item));
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::random;

    #[test]
    fn reverse_ord_inplace() {
        let slice = &mut [5, 4, 3, 2, 1];
        slice.mergesort();

        assert_eq!(&[1, 2, 3, 4, 5], slice);
    }

    #[test]
    fn reverse_ord() {
        let vec = vec![5, 4, 3, 2, 1];

        assert_eq!(&[1, 2, 3, 4, 5], vec.mergesorted().as_slice());
    }

    const BIG_SIZE: usize = 1_000_000;

    #[test]
    fn big_rand_inplace() {
        let mut vec: Vec<i32> = repeat_with(random).take(BIG_SIZE).collect();
        vec.mergesort();

        assert!(vec.windows(2).all(|window| match window {
            &[l, r] => l <= r,
            _ => unreachable!(),
        }));
    }
}
